# Bomb-Dino
A very silly game, probably full of bugs

## Usage
 - Clone the repo
 - ```make run```

## Some Variables (defined in the only source file)
 - FUSE:            The speed the circles are gonna collapse with
 - ACCEL:           "Player" acceleration
 - GRAVITY:         "Player" gravity
 - DRAG:            Some kinda drag force
 - EXHAUSTION:      Defines how fast your energy will drain
 - MAX_EXHAUSTION:  The players start "exhaustion". Changing this will lead to an incredibly ugly ... "HUD"
 - MAX_LIVES:       I think you should understand this
 - DAMAGE:          The damage a circle will do when its radius is smaller than EXPLODE
 - REGEN:           How fast HP is restored
 - EXPLODE:         The threshold for circle-damage

 And that's it. Have fun getting your mind blown up when either playing this game or reading its source.
 The total time I invested for this ... "project" ... is something about 2.5 hrs. ***DO NOT EXPECT ANYTHING TO WORK***
