#include <iostream>
#include <random>
#include <vector>
#include <algorithm>

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_scancode.h>
#include <SDL2/SDL_events.h>

#define FUSE 5.0f
#define ACCEL 0.35f
#define GRAVITY 0.3f
#define DRAG 0.01f
#define EXHAUSTION 1.0f
#define MAX_EXHAUSTION 400.0f
#define MAX_LIVES 400.0f
#define DAMAGE 20.0f
#define REGEN 0.3f
#define EXPLODE 350.0f
#define SPAWNRATE 35

void drawUnfilledCircle(SDL_Renderer* renderer, int centerX, int centerY, int radius) {
    int x = 0;
    int y = radius;
    int d = 3 - 2 * radius;

    while (x <= y) {
        SDL_RenderDrawPoint(renderer, centerX + x, centerY + y);
        SDL_RenderDrawPoint(renderer, centerX - x, centerY + y);
        SDL_RenderDrawPoint(renderer, centerX + x, centerY - y);
        SDL_RenderDrawPoint(renderer, centerX - x, centerY - y);
        SDL_RenderDrawPoint(renderer, centerX + y, centerY + x);
        SDL_RenderDrawPoint(renderer, centerX - y, centerY + x);
        SDL_RenderDrawPoint(renderer, centerX + y, centerY - x);
        SDL_RenderDrawPoint(renderer, centerX - y, centerY - x);

        if (d < 0) {
            d = d + 4 * x + 6;
        } else {
            d = d + 4 * (x - y) + 10;
            y--;
        }
        x++;
    }
}

bool touching(int rx, int ry, int w, int h, int cx, int cy, int r) {
    if (cx >= rx && cx <= rx + w && cy >= ry && cy <= ry + h) {
        return true;
    }

    int closestX = std::clamp(cx, rx, rx + w);
    int closestY = std::clamp(cy, ry, ry + h);

    int distanceX = closestX - cx;
    int distanceY = closestY - cy;

    if (std::hypot(distanceX, distanceY) <= r) {
        return true;
    }

    return false;
}

bool isKeyPressed(SDL_Scancode scancode) {
    const Uint8* keyboardState = SDL_GetKeyboardState(nullptr);
    return keyboardState[scancode];
}

void drawRect(SDL_Renderer* renderer, int x, int y, int w, int h) {
    SDL_RenderDrawLine(renderer, x, y, x + w, y);
    SDL_RenderDrawLine(renderer, x, y, x, y + h);
    SDL_RenderDrawLine(renderer, x + w, y, x + w, y + h);
    SDL_RenderDrawLine(renderer, x, y + h, x + w, y + h);
}

void getRainbowColor(float position, int& r, int& g, int& b) {
    float hue = position * 360.0f;
    float saturation = 1.0f;
    float value = 1.0f;

    float c = value * saturation;
    float x = c * (1 - std::abs(std::fmod(hue / 60.0f, 2.0f) - 1));
    float m = value - c;

    float r1, g1, b1;

    if (hue >= 0 && hue < 60) {
        r1 = c;
        g1 = x;
        b1 = 0;
    } else if (hue >= 60 && hue < 120) {
        r1 = x;
        g1 = c;
        b1 = 0;
    } else if (hue >= 120 && hue < 180) {
        r1 = 0;
        g1 = c;
        b1 = x;
    } else if (hue >= 180 && hue < 240) {
        r1 = 0;
        g1 = x;
        b1 = c;
    } else if (hue >= 240 && hue < 300) {
        r1 = x;
        g1 = 0;
        b1 = c;
    } else {
        r1 = c;
        g1 = 0;
        b1 = x;
    }

    r = static_cast<int>((r1 + m) * 255.0f);
    g = static_cast<int>((g1 + m) * 255.0f);
    b = static_cast<int>((b1 + m) * 255.0f);
}

struct bomb {
    float X = 0, Y = 0, r = 0, sr = 0;
};

int main(void) {
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("DinoCircle", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_RESIZABLE);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    int score = 0;

    bool quit = false;
    SDL_Event event;

    float posX = 0, posY = 0;
    float hspeed = 0, vspeed = 0;

    float exhaustionx = MAX_EXHAUSTION, exhaustiony = MAX_EXHAUSTION;
    bool exhaustedx = 0, exhaustedy = 0;

    float lives = MAX_LIVES;

    std::vector<bomb> bombs;

    std::random_device rd;
    std::mt19937 mt(rd());

    while (!quit) {
        bool dirkeyx = false;
        bool dirkeyy = false;

        int windowWidth, windowHeight;
        SDL_GetWindowSize(window, &windowWidth, &windowHeight);

        int centerX = floor(windowWidth / 2);
        int centerY = floor(windowHeight / 2);

        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            } else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                SDL_GetRendererOutputSize(renderer, &event.window.data1, &event.window.data2);
            } 
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);

        if(score % SPAWNRATE == 0) {
            bomb boom;
            std::uniform_real_distribution<double> coordX(0, windowWidth);
            std::uniform_real_distribution<double> coordY(0, windowHeight);
            boom.X = /*posX + windowWidth / 2.0;*/ coordX(mt);
            boom.Y = /*posY + windowHeight / 2.0;*/ coordY(mt);
            boom.r = 1000;
            boom.sr = 1000;
            bombs.push_back(boom);
        }

        for(bomb &x : bombs) {
            SDL_SetRenderDrawColor(renderer, (1 - (x.r / x.sr)) * 255, 0, (x.r / x.sr) * 255, 0);
            if(x.r < EXPLODE && touching(floor(centerX - 50 + posX), floor(centerY - 50 + posY), 50, 50, x.X, x.Y, x.r)) lives -= DAMAGE * (x.r / x.sr);
            drawUnfilledCircle(renderer, x.X, x.Y, x.r);
            x.r -= FUSE;
        }

        bombs.erase(std::remove_if(bombs.begin(), bombs.end(), [](const bomb& b) {return b.r <= 0;}), bombs.end());

        if(lives <= 0) {
            std::cout<<"U DED :)"<<std::endl<<"Ur score: "<<score<<std::endl;
            break;
        }

        lives += lives < MAX_LIVES ? REGEN : 0;

        SDL_SetRenderDrawColor(renderer, (1 - (exhaustionx / MAX_EXHAUSTION)) * 255, (exhaustionx / MAX_EXHAUSTION) * 255, 0, 0);
        SDL_Rect exX;
        exX.x = 0;
        exX.y = windowHeight - 40;
        exX.w = exhaustionx;
        exX.h = 20;
        SDL_RenderFillRect(renderer, &exX);

        SDL_SetRenderDrawColor(renderer, (1 - (exhaustiony / MAX_EXHAUSTION)) * 255, (exhaustiony / MAX_EXHAUSTION) * 255, 0, 0);
        SDL_Rect exY;
        exY.x = 0;
        exY.y = windowHeight - 20;
        exY.w = exhaustiony;
        exY.h = 20;
        SDL_RenderFillRect(renderer, &exY);

        SDL_SetRenderDrawColor(renderer, (1 - (lives / MAX_LIVES)) * 255, (lives / MAX_LIVES) * 255, 0, 0);
        SDL_Rect HP;
        HP.x = 0;
        HP.y = 0;
        HP.w = lives;
        HP.h = 20;
        SDL_RenderFillRect(renderer, &HP);

        const Uint8* keyboardState = SDL_GetKeyboardState(nullptr);
        if(!exhaustedx) {
            if (keyboardState[SDL_SCANCODE_D]) {
                hspeed += ACCEL;
                dirkeyx = true;
                exhaustionx -= EXHAUSTION;
            }
            if (keyboardState[SDL_SCANCODE_A]) {
                hspeed -= ACCEL;
                dirkeyx = true;
                exhaustionx -= EXHAUSTION;
            }
        }
        if(!exhaustedy) {
            if (keyboardState[SDL_SCANCODE_W]) {
                vspeed -= ACCEL;
                dirkeyy = true;
                exhaustiony -= EXHAUSTION;
            }
            if (keyboardState[SDL_SCANCODE_S]) {
                vspeed += ACCEL;
                dirkeyy = true;
                exhaustiony -= EXHAUSTION;
            }
        }

        exhaustedx = exhaustionx <= 0 ? true : (exhaustionx >= MAX_EXHAUSTION ? false : exhaustedx);
        exhaustedy = exhaustiony <= 0 ? true : (exhaustiony >= MAX_EXHAUSTION ? false : exhaustedy);

        if(posX < floor(windowWidth / 2) - 50 && posX > 0 - floor(windowWidth / 2) + 50) {
            posX += hspeed;
            if(!dirkeyx) {
                if(posX < -1) hspeed += GRAVITY;
                if(posX > 1) hspeed -= GRAVITY;
                if(hspeed < 0) hspeed += DRAG;
                if(hspeed > 0) hspeed -= DRAG;
                exhaustionx += exhaustionx < MAX_EXHAUSTION ? EXHAUSTION : 0;
            }
        } else {
            posX += posX < 0 ? 1 : -1;
            hspeed = 0;
        }
        if(posY < floor(windowHeight / 2) - 50 && posY > 0 - floor(windowHeight / 2) + 50) {
            posY += vspeed;
            if(!dirkeyy) {
                if(posY < -1) vspeed += GRAVITY;
                if(posY > 1) vspeed -= GRAVITY;
                if(vspeed < 0) vspeed += DRAG;
                if(vspeed > 0) vspeed -= DRAG;
                exhaustiony += exhaustiony < MAX_EXHAUSTION ? EXHAUSTION : 0;
            }
        } else {
            posY += posY < 0 ? 1 : -1;
            vspeed = 0;
        }

        int r, g, b;
        getRainbowColor(abs(static_cast<float>(posX + posY) / static_cast<float>(windowHeight + windowWidth)), r, g, b);
        SDL_SetRenderDrawColor(renderer, r, g, b, 255);

        drawRect(renderer, floor(centerX - 50 + posX), floor(centerY - 50 + posY), 100, 100);

        SDL_RenderPresent(renderer);

        score++;
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
